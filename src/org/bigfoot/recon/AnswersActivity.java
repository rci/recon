package org.bigfoot.recon;

import java.util.Set;
import android.util.Log;

import android.os.Bundle;

import android.content.Intent;

import android.app.Activity;

import android.view.Window;
import android.view.View;
import android.view.View.OnClickListener;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class AnswersActivity extends Activity
{
  private ArrayAdapter<String> _answersArrayAdapter;
  private ListView _answersListView;
  private final String _logTag = "answerListActivity";
  
  // Return Intent extra
  public static String EXTRA_ANSWER = "answer";
  private String _title;
  private String[] _answers = null;

  // The on-click listener for all devices in the ListViews
  private OnItemClickListener _answersClickListener = new OnItemClickListener()
  {
    public void onItemClick(AdapterView<?> av, View v, int arg2, long arg3)
    {
      // Get the device MAC address, which is the last 17 chars in the View
      String answer = ((TextView) v).getText().toString();

      // Create the result Intent and include the MAC address
      Intent intent = new Intent();
      intent.putExtra(EXTRA_ANSWER, answer);

      // Set result and finish this Activity
      setResult(Activity.RESULT_OK, intent);
      finish();
    }
  };

  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    Log.d( _logTag, "onCreate");

    super.onCreate(savedInstanceState);

    // Setup the window
    requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);

    setContentView(R.layout.answer_list);

    // initialize paired devices array adapter
    _answersArrayAdapter = new ArrayAdapter<String>(this, R.layout.list_element);
    _answersListView = (ListView) findViewById(R.id.vAnswers);
    _answersListView.setAdapter( _answersArrayAdapter );
    _answersListView.setOnItemClickListener( _answersClickListener );

    // sets the custom title
    Bundle b = getIntent().getExtras();
    _title = b.getString("title");
    _answers = b.getStringArray("answers");
    for( int i = 0; i < _answers.length; ++i )
      _answersArrayAdapter.add(_answers[i]);
    
    getWindow().setFeatureInt( Window.FEATURE_CUSTOM_TITLE, R.layout.title_main );
    TextView titleText = (TextView) findViewById(R.id.titleMain);
    titleText.setText( _title );

    // Set result CANCELED incase the user backs out
    setResult(Activity.RESULT_CANCELED);
    
  }
}

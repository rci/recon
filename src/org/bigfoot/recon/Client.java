package org.bigfoot.recon;

import java.util.UUID;
import java.io.IOException;
import java.lang.InterruptedException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;

import android.bluetooth.BluetoothSocket;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothDevice;
import android.os.ParcelUuid;

import android.util.Log;

public abstract class Client
{
  private static final String _logTag = "ReconClient";
  private BluetoothAdapter _bluetoothAdapter;
  private BluetoothDevice  _bluetoothDevice;
  private BluetoothSocket  _clientSocket = null;
  private InputStream      _clientInStream = null;
  private OutputStream     _clientOutStream = null;
  private UUID _serviceUUID;
  
  public enum Status
  {
    NOT_INITIALIZED,
    CONNECTED,
    DISCONNECTED
  }
  
  public enum Actions
  {
    NONE,
    INITIALIZING,
    LISTENING,
    DISCONNECTING
  }
  
  private Status _status = Status.NOT_INITIALIZED;
  private Actions _action = Actions.NONE;

  synchronized Status getStatus() {return _status;}
  synchronized void setStatus( Status inStatus ) { _status = inStatus;
                          Log.d( _logTag, "setStatus " + inStatus.toString());}
  synchronized Actions getAction() {return _action;}
  synchronized void setAction( Actions inAction ) { _action = inAction;
                          Log.d( _logTag, "setAction " + inAction.toString());}

  private class ConnectThread extends Thread
  {
    boolean _hostDown = false;  // true if the host is down
    boolean _checkOk = false;   // true if the connection succeeded
    synchronized public void run()
    {
      boolean ok = true;

      
      try {
        _clientSocket = _bluetoothDevice.createRfcommSocketToServiceRecord( _serviceUUID );
        Log.d( _logTag, "socket created " + _clientSocket );
      } catch (IOException e) {
        Log.e( _logTag, "socket creation failed", e);
        ok = false;
      }

      if( ok )
      {
        try{
            _bluetoothAdapter.cancelDiscovery();
            _clientSocket.connect();
            Log.d( _logTag, "socket connected");
            
        
        } catch (IOException e) {
            Log.e( _logTag, "connection failed", e);
            ok = false;
        }
      }

      if( ok )
      {
        try{
          _clientInStream  = _clientSocket.getInputStream();
          _clientOutStream = _clientSocket.getOutputStream();
        }
        catch (IOException e) {ok = false;}
      }
      
      if(ok)
      {
        setStatus( Status.CONNECTED );
        _listenThread = new ListenThread();
        _listenThread.start();
        onConnect();
      }
      else
      {
        setStatus( Status.DISCONNECTED );
        closeSocket();
        onFailure();
      }
    }
    
    void closeSocket()
    {
      _clientInStream = null;
      _clientOutStream = null;
      if( _clientSocket != null)
      {
        Log.d( _logTag, "channel = " + _clientSocket);
        try{ _clientSocket.close(); } catch (IOException e) {}
      }
      _clientSocket = null;
    }
    
    boolean secondaryConnectionMode()
    {
      boolean ok = false;
      Method m = null;
      try{
        m = _bluetoothDevice.getClass().getMethod("createRfcommSocket", new Class[] {int.class});
        // checks that the method can be called
        _clientSocket = (BluetoothSocket) m.invoke(_bluetoothDevice, 1);
        ok = true;
      }
      catch (Exception e){
        Log.e( _logTag, "second attempt failed, method probably unsupported", e);
        ok = false;
      }
      if(ok)
      {
        ok = false;
        int channel = 0;
        _checkOk = false;
        for( channel = 1; (channel < 31) && (ok == false) && (_hostDown == false); ++channel )
        {
          try{
            _clientSocket = (BluetoothSocket) m.invoke(_bluetoothDevice, channel);
            
            try{
              _clientSocket.connect();
              _clientInStream  = _clientSocket.getInputStream();
              _clientOutStream = _clientSocket.getOutputStream();
              
              //read incomming data from the connection
              CheckingThread checkingThread = new CheckingThread();
              checkingThread.start();
              try{
                checkingThread.join(500);
              } catch (InterruptedException e){ Log.d( _logTag, "check thread interrupted"); }
              
              if(!_checkOk)
              {
                Log.d( _logTag, "wrong channel " + channel);
              }
              ok = _checkOk;
            }
            catch (IOException e)
            {
              Log.e( _logTag, "couldn't get client socket for channel " + channel, e);
              ok = false;
            }
          }
          catch(Exception e)
          {
            Log.e( _logTag, "couldn't check channel " + channel, e);
            if( e.getMessage().equals("Host is down") )
              _hostDown = true;
            ok = false;
          }
          // if something went wrong, close the socket
          if( !ok )
            closeSocket();
        }
      }
      return ok;
    }
    
    // check if the connected channel is the correct one
    private class CheckingThread extends Thread
    {
      public void run()
      {
        boolean ok = true;
        byte[] tmpData = new byte[0];
        while( ok )
        {
          // read data from the device
          // the data received should be the service UUID
          if( _clientInStream != null )
          {
            int size = -1;
            byte[] buffer = new byte[1024];
            try{ size = _clientInStream.read( buffer ); }
            catch (IOException e)
            {
              Log.e( _logTag, "couldn't test the channel", e);
              ok = false;
            }
            
            if( size > 0 )
            {
              byte[] newArray = new byte[tmpData.length + size];
              System.arraycopy( tmpData, 0, newArray, 0, tmpData.length );
              System.arraycopy(  buffer, 0, newArray, tmpData.length, size );
              tmpData = newArray;
              Log.d( _logTag, new String(tmpData));
              String value = "";
              for( int i = 0; i < tmpData.length; ++i)
              {
                value += ((int)tmpData[i]) + ".";
              }
              Log.d( _logTag, value);
            }
            // once all the data are received, check the UUID
            if( tmpData.length >= 16)
            {
              byte[] HEX_CHAR_TABLE = {
                (byte)'0', (byte)'1', (byte)'2', (byte)'3',
                (byte)'4', (byte)'5', (byte)'6', (byte)'7',
                (byte)'8', (byte)'9', (byte)'a', (byte)'b',
                (byte)'c', (byte)'d', (byte)'e', (byte)'f'};
              String strUUID = "";
              System.arraycopy( tmpData, 0, tmpData, 0, 16);
              for( int i = 0; i < tmpData.length; ++i)
              {
                strUUID += (char)HEX_CHAR_TABLE[ (int)((0xf0 & tmpData[i]) >> 4) ];
                strUUID += (char)HEX_CHAR_TABLE[ (int)(0x0f & tmpData[i])];
                if(i == 3 || i == 5 || i == 7 || i == 9)
                  strUUID += '-';
              }
              Log.d( _logTag, strUUID);
              UUID testUUID = UUID.fromString( strUUID );
              Log.d( _logTag, testUUID.toString());
              Log.d( _logTag, _serviceUUID.toString());
              
              if( strUUID.equals(_serviceUUID.toString()) )
              {
                Log.d( _logTag, "channel ok" );
                _checkOk = true;
              }
              ok = false;
            }
          }
        }
      }
    }
  }
  
  /// connects the device to the server
  private ConnectThread _connectThread = null;
  // end connect thread
  
  private class ListenThread extends Thread
  {
    public void run()
    {
      byte[] buffer = new byte[1024];
      
      if( getStatus() == Status.CONNECTED)
        setAction( Actions.LISTENING );
      while(( getAction() == Actions.LISTENING ) && ( getStatus() == Status.CONNECTED ))
      {
        if( _clientInStream != null )
        {
          int size = -1;
          try{ size = _clientInStream.read( buffer ); }
          catch (IOException e)
          {
            setAction( Actions.NONE );
            Log.d( _logTag, "disconnected");
          }
          
          if( size > 0 )
            onReceive( buffer, size );
        }
        else
          setAction( Actions.NONE );
      }
      Log.d( _logTag, "listening stopped");
    }
  }
  
  /// listen the server
  private ListenThread _listenThread = null;

  public void connect( String inAddress, UUID inServiceUUID, BluetoothAdapter inBluetoothAdapter )
  {
    disconnect();
    setAction( Actions.INITIALIZING );
    _bluetoothAdapter = inBluetoothAdapter;
    _bluetoothDevice = inBluetoothAdapter.getRemoteDevice( inAddress );
    _serviceUUID = inServiceUUID;

    onConnecting();
    _connectThread = new ConnectThread();
    _connectThread.start();
  }

  /// closing device
  public void close()
  {
    disconnect();
  }
  
  /// disconnects the device
  private void disconnect()
  {
    if( getStatus() == Status.CONNECTED )
    {
      Log.d( _logTag, "disconnecting");
      setAction( Actions.DISCONNECTING );
      
      // close streams
      if( _clientInStream != null)
        { try{ _clientInStream.close(); Log.d( _logTag, "inStream closed");} catch(IOException e) {} }
      if( _clientOutStream != null)
        { try{ _clientOutStream.close(); Log.d( _logTag, "outStream closed");} catch(IOException e) {} }
      _clientInStream = null;
      _clientOutStream = null;
      
      // close socket
      if( _clientSocket != null )
        { try{ _clientSocket.close(); Log.d( _logTag, "socket closed");} catch(IOException e) {} }
      _clientSocket = null;
      

      // stop listening
      if( _listenThread != null )
        { try{ _listenThread.join(); } catch(InterruptedException e) {} }
      _listenThread = null;
      Log.d( _logTag, "listening stopped");

      Log.d( _logTag, "disconnected");
      onDisconnect();
    }
    setStatus( Status.DISCONNECTED );
  }
  
  /// write data to the device
  synchronized public void write( byte[] buffer ) throws IOException
  {
    if( getStatus() == Status.CONNECTED)  
      _clientOutStream.write(buffer);
  }
  
  /// called when data are incoming
  public abstract void onReceive( byte[] inData, int inSize );

  /// called when the client is disconnected
  public abstract void onDisconnect();

  /// called when starting a connection attempt
  public abstract void onConnecting();

  /// called if the connection failed
  public abstract void onFailure();

  /// called when the connection succeded
  public abstract void onConnect();
}



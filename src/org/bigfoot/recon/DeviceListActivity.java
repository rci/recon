package org.bigfoot.recon;

import java.util.Set;
import android.util.Log;

import android.os.Bundle;

import android.content.Intent;

import android.app.Activity;

import android.view.Window;
import android.view.View;
import android.view.View.OnClickListener;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;

public class DeviceListActivity extends Activity
{
  private ArrayAdapter<String> _pairedDevicesArrayAdapter;
  private ListView _devicesListView;
  private BluetoothAdapter _bluetoothAdapter;
  private final String _logTag = "deviceListActivity";
  
  // Return Intent extra
  public static String EXTRA_DEVICE_ADDRESS = "device_address";

  // The on-click listener for all devices in the ListViews
  private OnItemClickListener _devicesClickListener = new OnItemClickListener()
  {
    public void onItemClick(AdapterView<?> av, View v, int arg2, long arg3)
    {
      // Get the device MAC address, which is the last 17 chars in the View
      String info = ((TextView) v).getText().toString();
      String address = info.substring(info.length() - 17);

      // Create the result Intent and include the MAC address
      Intent intent = new Intent();
      intent.putExtra(EXTRA_DEVICE_ADDRESS, address);

      // Set result and finish this Activity
      setResult(Activity.RESULT_OK, intent);
      finish();
    }
  };

  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);

    // Setup the window
    requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
    setContentView(R.layout.data_list);

    // Set result CANCELED incase the user backs out
    setResult(Activity.RESULT_CANCELED);
    
    // initialize paired devices array adapter
    _pairedDevicesArrayAdapter = new ArrayAdapter<String>(this, R.layout.list_element);
    _devicesListView = (ListView) findViewById(R.id.vList);
    _devicesListView.setAdapter( _pairedDevicesArrayAdapter );
    _devicesListView.setOnItemClickListener( _devicesClickListener);
    
    // get paired devices
    _bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    Set<BluetoothDevice> pairedDevices = _bluetoothAdapter.getBondedDevices();

    // add deviceses to the array adapter
    if (pairedDevices.size() > 0)
    {
      for (BluetoothDevice device : pairedDevices)
      {
        _pairedDevicesArrayAdapter.add(device.getName() + "\n" + device.getAddress());
      }
    }
  }
}

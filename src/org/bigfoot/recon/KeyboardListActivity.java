package org.bigfoot.recon;

import android.util.Log;

import android.os.Bundle;

import android.content.Intent;

import android.app.Activity;

import android.view.Window;
import android.view.View;
import android.view.View.OnClickListener;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class KeyboardListActivity extends Activity
{
  private ArrayAdapter<String> _availableKeyboardsArrayAdapter;
  private ListView _keyboardsListView;
  private final String _logTag = "keyboardListActivity";
  
  int _nDefaultKeyboards = 0;

  // Return Intent extra
  public static String EXTRA_KEYBOARD_NAME = "keyboardName";

  void initKeyboardList()
  {
    // init the list objects
    _availableKeyboardsArrayAdapter = new ArrayAdapter<String>(this, R.layout.list_element);
    _keyboardsListView = (ListView) findViewById(R.id.vList);
    _keyboardsListView.setAdapter( _availableKeyboardsArrayAdapter );
    _keyboardsListView.setOnItemClickListener( _itemClickListener );

    // add keyboards to the list
    _availableKeyboardsArrayAdapter.add("Player Basic");
    _availableKeyboardsArrayAdapter.add("Digit Basic");
    _nDefaultKeyboards = _availableKeyboardsArrayAdapter.getCount();
    // some code to find the installed plugins

  }
  
  // The on-click listener for all elements in the listview
  private OnItemClickListener _itemClickListener = new OnItemClickListener()
  {
    public void onItemClick(AdapterView<?> av, View v, int arg2, long arg3)
    {
      // Get displayed text
      String info = ((TextView) v).getText().toString();

      // Create the result Intent and include the extracted texte
      Intent intent = new Intent();
      intent.putExtra(EXTRA_KEYBOARD_NAME, info);

      // Set result and finish this Activity
      setResult(Activity.RESULT_OK, intent);
      finish();
    }
  };

  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);

    // Setup the window
    requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
    setContentView(R.layout.data_list);
 
    // Set result CANCELED in case the user backs out
    setResult(Activity.RESULT_CANCELED);
    
    // Sets the available keyboards list
    initKeyboardList();
  }
}






package org.bigfoot.recon;


import java.nio.ByteBuffer;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;

import android.app.Activity;

import android.view.Window;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.InflateException;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import android.graphics.Color;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.Context;

import android.view.ViewGroup;

import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ImageButton;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import android.util.AttributeSet;
import android.util.Log;

public class Recon extends Activity
{
  /// commands Emitted (EC) to the device
  private static final byte EC_CHANGE_STATE = 1; ///< sent to control the programm running on the device
  private static final byte EC_ARGUMENT_DATA = 2; ///< indicate an argument data
  /// commands received (RC) by the Remote Controller (phone)
  private static final byte RC_PRINTABLE_DATA = 1; ///< indicates that a printable data is received
  private static final byte RC_ANSWER_DATA = 2; ///< indicates that a printable data is received
  private static final byte RC_PRINTABLE_ACTION = 3; ///< action associated to a printable value
  private static final byte RC_ANSWER_ACTION = 4; ///< action associated with the answer received
  private static final byte RC_CLEAR  = 5; ///< action associated with the answer received
  
  static private Recon _mainActivity = null;
  
  public ArrayAdapter<String> _paramsArrayAdapter;
  public ArrayAdapter<String> _paramsActionArrayAdapter;
  
  private int _paramsMax = 10; ///< maximum number of parameters to display
  private ListView _paramsView;
  private ReconBluetooth _bluetoothServer;
  private String[] _answers = null;
  private String _answerTitle;

  private static final int AA_NONE = 0;
  private static final int AA_SEND_COMMAND = 1;
  private static final int AA_SET_ACTIVE_PLUGIN = 2;
  private int _answerAction = AA_SEND_COMMAND;
  private String _answerCommand;
  private String _activePlugin = "";
  private int _argIndex = 0;

  public Recon()
  {
    super();
    _mainActivity = this;
  }

  static public class CommandButton extends ReconCommandButton
  {
	  public CommandButton(Context context, AttributeSet attrs)
	  {
	    super(context, attrs);
	  }
	
	  public CommandButton(Context context, AttributeSet attrs, int defStyle)
	  {
	    super(context, attrs, defStyle);
	  }

  	@Override
  	protected void setAttributes(Context context, AttributeSet attrs)
  	{
  	  super.setAttributes(context, attrs);
  	  setOnClickListener(Recon._mainActivity._onClickListenerMain);
  	}
  }

  ///\handle button clicks
  private OnClickListener _onClickListenerMain = new OnClickListener()
  {
    public void onClick(View v)
    {
      byte[] bluetoothMessage = null;
      String msg = null;

      ReconCommandButton cmdButton = (ReconCommandButton) v;
      
      msg = _activePlugin + "." + cmdButton.getAction();

      int nArguments = cmdButton.getArgument() == ""? 0:1;
      
      if( msg != null)
      {
        bluetoothMessage = makeMessage( EC_CHANGE_STATE, msg.getBytes(), nArguments );
        _bluetoothServer.writeData( bluetoothMessage );
        if(nArguments != 0)
        {
          bluetoothMessage = makeMessage( EC_ARGUMENT_DATA, cmdButton.getArgument().getBytes(), nArguments);
          _bluetoothServer.writeData( bluetoothMessage);
        }
      }
    }
  };
  private static final String _logTag = "Recon";
  
  // on activity result codes
  public static final int ACTIVATE_BLUETOOTH = 1;
  public static final int REQUEST_DEVICE_CONNECTION = 2;
  public static final int ANSWER_SELECTED = 3;
  public static final int SELECT_KEYBOARD = 4;
  
  private Handler _handlerBluetoothServer = new ReconBluetoothHandler() 
  {
    public void handleMessage( byte[] inMsg )
    {
      boolean msgEmpty = true;
      byte cmd = 0;
      if( inMsg.length != 0 )
          cmd = inMsg[0];
      switch( cmd )
      {
        case RC_PRINTABLE_DATA:
          printData( inMsg, inMsg.length );
          break;
        case RC_ANSWER_DATA:
          getAnswer( inMsg, inMsg.length);
          break;
        case RC_PRINTABLE_ACTION:
          setPrintableAction( inMsg, inMsg.length );
          break;
        case RC_ANSWER_ACTION:
          _answerCommand = "";
          if( inMsg.length > 2 )
            _answerCommand = new String(inMsg, 1, inMsg.length - 1);
          Log.d( _logTag, "answerCommand: " + _answerCommand);
          break;
        case RC_CLEAR:
          Log.d( _logTag, "clear screen");
          _paramsActionArrayAdapter.clear();
          _paramsArrayAdapter.clear();
          break;
        default:
          break;
      }
    }

    @Override
    protected void onStatusChange( String inText, int inColor )
    {
      setStatus( inText, inColor );
    }
    
    @Override
    protected void onMessageDisplay( String inText)
    {
      if( inText != "" )
        Toast.makeText( getApplicationContext(), inText, Toast.LENGTH_SHORT ).show();
    }
    
    @Override
    protected void onBluetoothReception( byte[] data )
    {
      handleMessage( data );
    }
  };

  /** Called when the activity is first created. */
  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    Log.d( _logTag, "onCreate");

    super.onCreate(savedInstanceState);

    // requests the custom title feature
    requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);

    setContentView(R.layout.main);
    
    // sets the custom title
    getWindow().setFeatureInt( Window.FEATURE_CUSTOM_TITLE, R.layout.title_main );

    // loads the default commands layout
    setKeyboard(R.layout.defaultcommands);
        
    TextView titleText = (TextView) findViewById(R.id.titleMain);
    if(titleText != null)
      titleText.setText( R.string.app_name );
    
    setStatus( "Disconnected", Color.RED );
    
    _bluetoothServer = new ReconBluetooth( this, _handlerBluetoothServer );
  }
  
  @Override
  public void onStart()
  {
    super.onStart();
    setupRecon();
  }
  
  @Override
  public void onRestart()
  {
    super.onRestart();
    _bluetoothServer.reconnect();
  }

  @Override
  public void onResume()
  {
    super.onResume();
    _bluetoothServer.bluetoothActivated();
  }
  
  @Override
  public void onStop()
  {
    super.onStop();
    _bluetoothServer.closeBluetooth();
  }

  @Override
  public void onDestroy()
  {
    Log.d( _logTag, "onDestroy");
    super.onDestroy();
  }

  @Override
  public void onConfigurationChanged(Configuration newConfig)
  {
    super.onConfigurationChanged(newConfig);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu)
  {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.main_menu, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    switch (item.getItemId())
    {
    case R.id.scan:
      // Launch the DeviceListActivity to see devices and do scan
      Intent serverIntent = new Intent(this, DeviceListActivity.class);
      startActivityForResult(serverIntent, REQUEST_DEVICE_CONNECTION);
      return true;
    case R.id.keyboard:
      // Selects the Keyboard
      Intent keyboardIntent = new Intent(this, KeyboardListActivity.class);
      startActivityForResult(keyboardIntent, SELECT_KEYBOARD);
      return true;
    case R.id.pluginList:
      String msg = "getPlugins";
      byte[] bluetoothMessage = null;
      _answerAction = AA_SET_ACTIVE_PLUGIN;
      _answerCommand = null;
      bluetoothMessage = makeMessage( EC_CHANGE_STATE, msg.getBytes(), 0 );
      _bluetoothServer.writeData( bluetoothMessage );
      return true;
    default:
      return false;
    }
  }
  
  public void setStatus( String inStatus, int color )
  {
    TextView statusText = (TextView) findViewById(R.id.statusMain);
    if( statusText != null )
    {
      statusText.setText( inStatus );
      statusText.setTextColor( color );
    }
  }
  
  private void setKeyboard(int inView)
  {
    ViewGroup commandView = (ViewGroup) findViewById(R.id.commandsContainer);
    commandView.removeAllViews();
    
    try{
      LayoutInflater.from(getBaseContext()).inflate(inView, commandView, true);
    } catch (InflateException e) { Log.e(_logTag, "Inflate Failed", e); }
  }
  
  private void setKeyboard(String keyboardName)
  {
    Log.d(_logTag, keyboardName);
    
    if( keyboardName.equals("Player Basic") )
      setKeyboard( R.layout.defaultcommands );
    else if( keyboardName.equals("Digit Basic") )
      setKeyboard( R.layout.tvcommands );    
    else
    {
      // add some code to load plugin keyboards
      Log.d(_logTag, "keyboard not found");
    }
  }
  
  private void setupRecon()
  {
    if( _paramsArrayAdapter == null )
    {
      _paramsArrayAdapter = new ArrayAdapter<String>(this, R.layout.list_element);
      _paramsActionArrayAdapter = new ArrayAdapter<String>(this, R.layout.list_element);
      _paramsView = (ListView) findViewById(R.id.vDisplayedParams);
      _paramsView.setAdapter( _paramsArrayAdapter );
      _paramsView.setOnItemClickListener( new OnItemClickListener() {
          public void onItemClick(AdapterView<?> av, View v, int arg2, long arg3)
          {
            if( arg2 < _paramsActionArrayAdapter.getCount() )
            {
              Log.d( _logTag, "item position " + arg2);
              String action = _activePlugin + "." + _paramsActionArrayAdapter.getItem(arg2);
              _answerAction = AA_SEND_COMMAND;
              _bluetoothServer.writeData( makeMessage( EC_CHANGE_STATE, action.getBytes(), 0 ) );
            }
          }
        });
    }
  }
  
  private void printData( byte[] inMsg, int size )
  {
    byte position;
    if( inMsg.length > 3 )
    {
      position = inMsg[1];
      if( position < _paramsMax )
      {
        while(position >= _paramsArrayAdapter.getCount())
          _paramsArrayAdapter.add( new String() );
        
        String msg = new String(inMsg, 2, size - 2);
        _paramsArrayAdapter.remove( _paramsArrayAdapter.getItem( (int)position ) );
        _paramsArrayAdapter.insert( msg, (int)position );
      }
    }
  }
  
  byte[]  makeMessage( int fnct, byte[] inMsg, int nArguments )
  {
    int position = 0;
    byte[] outMsg;
    if(fnct == EC_CHANGE_STATE)
    {
      Log.d( _logTag, "Change State");

      _argIndex = 0;
      outMsg = new byte[ inMsg.length + ReconBluetooth.SIZE_BYTES + 1];
      outMsg[position] = (char)EC_CHANGE_STATE;
      ++position;
      
      ByteBuffer byteBuffer = ByteBuffer.allocate( ReconBluetooth.SIZE_BYTES );
      byteBuffer.putInt( nArguments );
      for(int i = 0; i < ReconBluetooth.SIZE_BYTES; ++i)
        outMsg[ position + i ] = byteBuffer.array()[i];
      position += ReconBluetooth.SIZE_BYTES;
    }
    else if(fnct == EC_ARGUMENT_DATA)
    {
      Log.d( _logTag, "Argument");

      outMsg = new byte[ inMsg.length + ReconBluetooth.SIZE_BYTES + ReconBluetooth.SIZE_BYTES + 1];
      outMsg[position] = (char)EC_ARGUMENT_DATA;
      ++position;
      
      ByteBuffer byteBufferIndex = ByteBuffer.allocate( ReconBluetooth.SIZE_BYTES );
      byteBufferIndex.putInt( _argIndex );

      for(int i = 0; i < ReconBluetooth.SIZE_BYTES; ++i)
        outMsg[ position + i ] = byteBufferIndex.array()[i];
      position += ReconBluetooth.SIZE_BYTES;

      ByteBuffer byteBufferNArgs = ByteBuffer.allocate( ReconBluetooth.SIZE_BYTES );
      byteBufferNArgs.putInt( nArguments );

      for(int i = 0; i < ReconBluetooth.SIZE_BYTES; ++i)
        outMsg[ position + i ] = byteBufferNArgs.array()[i];
      position += ReconBluetooth.SIZE_BYTES;

      ++_argIndex;
    }
    else
    {
      outMsg = inMsg;
    }
    for(int i = 0; i < inMsg.length; ++i)
      outMsg[ position + i ] = inMsg[i];

    return outMsg;
  }
  
  synchronized private void getAnswer( byte[] inMsg, int size )
  {

    int position = 0;
    for ( int i = 0; i < 4; ++i )
      position = ( position << 8 ) + ( inMsg[i + 1] & 0xff );
    
    int nValues = 0;
    for ( int i = 0; i < 4; ++i )
      nValues = ( nValues << 8 ) + ( inMsg[i + 5] & 0xff );

    String value = new String(inMsg, 9, size - 9);

    Log.d( _logTag, "answer " + Integer.toString(position) + " on " + Integer.toString(nValues) + " " + value );
    if( position == 0 )
    {
      _answers = new String[nValues];
    }        
    _answers[position] = value;

    if( position == nValues - 1 )
    {
      Intent answerIntent = new Intent(this, AnswersActivity.class);
      Bundle b = new Bundle();
      b.putString("title", _answerTitle);
      b.putStringArray("answers", _answers);
      answerIntent.putExtras(b);
      startActivityForResult( answerIntent, ANSWER_SELECTED );
    }
  }
  
  public void setPrintableAction( byte[] inMsg, int size )
  {
    byte position;
    if( inMsg.length > 3 )
    {
      position = inMsg[1];
      if( position < _paramsMax )
      {
        while(position >= _paramsActionArrayAdapter.getCount())
          _paramsActionArrayAdapter.add( new String() );
        
        String msg = new String(inMsg, 2, size - 2);
        _paramsActionArrayAdapter.remove( _paramsActionArrayAdapter.getItem( (int)position ) );
        _paramsActionArrayAdapter.insert( msg, (int)position );
      }
    }
  }

  public void onActivityResult(int requestCode, int resultCode, Intent data)
  {
    switch (requestCode)
    {
      case ACTIVATE_BLUETOOTH:
        _bluetoothServer.answerBluetoothRequest( resultCode == Activity.RESULT_OK );
        if( resultCode != Activity.RESULT_OK)
        {
          Toast.makeText(this, "bluetooth not enabled", Toast.LENGTH_SHORT).show();
          finish();
        }
        break;
      case REQUEST_DEVICE_CONNECTION:
        if( resultCode == Activity.RESULT_OK )
        {
          // Get the device MAC address
          String address = data.getExtras()
                               .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
          String message = "Connecting to the device: " + address;
          Toast.makeText( this, message, Toast.LENGTH_SHORT).show();
          _bluetoothServer.setBluetoothDeviceFromAddress( address );
        }
        break;
      case SELECT_KEYBOARD:
        if( resultCode == Activity.RESULT_OK )
        {
          String keyboard = data.getExtras()
                                .getString(KeyboardListActivity.EXTRA_KEYBOARD_NAME);
          setKeyboard(keyboard);
        }
      case ANSWER_SELECTED:
        if( resultCode == Activity.RESULT_OK )
        {
          switch (_answerAction)
          {
            case AA_SEND_COMMAND:
              if( _answerCommand != null )
              {
                String actionRequest = _activePlugin + ".actionRequest";
                
                _bluetoothServer.writeData( makeMessage( EC_CHANGE_STATE, actionRequest.getBytes(), 2 ) );
                
                _bluetoothServer.writeData( makeMessage( EC_ARGUMENT_DATA, _answerCommand.getBytes(), 2 ) );

                String answer = data.getExtras().getString(AnswersActivity.EXTRA_ANSWER);
                _bluetoothServer.writeData( makeMessage( EC_ARGUMENT_DATA, answer.getBytes(), 2 ) );
              }
            break;
            case AA_SET_ACTIVE_PLUGIN:
              _activePlugin = data.getExtras().getString(AnswersActivity.EXTRA_ANSWER);
              Toast.makeText( this,"now controling:\n"+_activePlugin, Toast.LENGTH_SHORT).show();
            break;
            default:
            break;
          }
          Log.d( _logTag, "answer action : " + Integer.toString(_answerAction));
          _answerAction = AA_SEND_COMMAND;
          _answerCommand = null;
        }
        break;
      default:
        break;
    }
  }
}

package org.bigfoot.recon;

import java.util.UUID;
import java.util.Arrays;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.nio.ByteBuffer;

import android.view.View;

import android.app.Activity;
import android.app.ProgressDialog;

import android.widget.ProgressBar;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
 	
import android.content.Intent;
import android.content.IntentFilter;
import android.content.BroadcastReceiver;
import android.content.Context;

import android.bluetooth.BluetoothSocket;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothDevice;

import android.util.Log;

public class ReconBluetooth
{
  private static final String _logTag = "ReconBluetooth";
  private final Activity _activity;
  private final Handler _handler;

  public static final int SIZE_BYTES = 4;
  private static final int BUFFER_SIZE = 1024;
  private static final int MAX_SIZE = 1024;
  private static final UUID _serviceUUID = UUID.fromString("c4999490-84d8-11e1-981a-000df059723e");
  private static final String _hciName = "Recon";
  private static BluetoothAdapter _bluetoothAdapter;
  private byte[] _incomingData = null;
  private int _incomingPacketSize = 0;
  private ProgressDialog _progDlg = null;
  private String _lastConnectedAddr = null;
  private String _lastAttemptAddr = null;
  
  private Client _client = new Client() {
    @Override
    public void onReceive( byte[] inData, int inSize )
    { packetHandler( inData, inSize ); }
    
    @Override
    public void onDisconnect()
    { _handler.obtainMessage( ReconBluetoothHandler.MESSAGE_CONNECTION_LOST, 0, -1 )
              .sendToTarget(); }
    
    @Override
    public void onConnect()
    { _handler.obtainMessage( ReconBluetoothHandler.MESSAGE_CONNECTED, 0, -1 )
              .sendToTarget();
      _lastConnectedAddr = _lastAttemptAddr;
      _progDlg.dismiss();
    }
    
    @Override
    public void onConnecting()
    {
      _handler.obtainMessage( ReconBluetoothHandler.MESSAGE_CONNECTING, 0, -1 )
              .sendToTarget();
      _progDlg.setProgress(0);
      _progDlg.show();
    }
    @Override
    public void onFailure()
    { _handler.obtainMessage( ReconBluetoothHandler.MESSAGE_CONNECTING_FAILED, 0, -1 )
              .sendToTarget();
      _lastConnectedAddr = null;
      _progDlg.dismiss();
    }
  };
  
  // listen to bluetooth status change
  private BroadcastReceiver _bluetoothStatusReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent)
    {
      if( intent.getAction().equals( BluetoothAdapter.ACTION_STATE_CHANGED ))
      {
        int state = intent.getExtras().getInt( BluetoothAdapter.EXTRA_STATE );
        if( state == BluetoothAdapter.STATE_ON )
          bluetoothActivated();
        else if( state == BluetoothAdapter.STATE_TURNING_OFF )
          bluetoothDisabled();
      }
      else if( intent.getAction().equals( BluetoothDevice.ACTION_ACL_DISCONNECTED ))
        _client.close();
    }
  };

  public enum InitStep
  {
    NOT_INITIALIZED,
    GET_BLUETOOTH_ADAPTER,
    CHECK_BLUETOOTH_ENABLED,
    START_RECON_SERVICE,
    INIT_FINISHED
  }
  
  private InitStep _initializationStep = InitStep.NOT_INITIALIZED;
  
  private boolean _intentBTStatusRegistered = false;
  
  /**
   * Constructor. Prepares a new Bluetooth session.
   * @param handler  A Handler to send messages back to the UI Activity
   */
  public ReconBluetooth( Activity activity, Handler handler )
  {
    _activity = activity;
    _handler = handler;
    
    _progDlg = new ProgressDialog(_activity){
      @Override
      protected void onCreate(Bundle s)
      {
        super.onCreate(s);
        setMessage("Connecting...");
      }
      @Override
      public void onStart()
      {
        super.onStart();
        ProgressBar p = (ProgressBar) this.findViewById(android.R.id.progress);
            p.setVisibility(View.GONE);
            p.setVisibility(View.VISIBLE);
      }
      
    };
    initBluetooth();
  }

  public void bluetoothActivated()
  {
    initBluetooth();
  }
  
  public void bluetoothDisabled()
  {
    closeBluetooth();
  }
  
  private boolean getBluetoothAdapter()
  {
    boolean ans = true;
    
    Log.d( _logTag, "getBluetoothAdapter");
    
    _bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    if( _bluetoothAdapter == null )
    {
      _handler.obtainMessage( ReconBluetoothHandler.MESSAGE_NO_BLUETOOTH, 0, -1 ).sendToTarget();
      ans = false;
    }
    else
    {
      if( _intentBTStatusRegistered == false )
      {
        IntentFilter filter = new IntentFilter();
        filter.addAction( BluetoothAdapter.ACTION_STATE_CHANGED );
        filter.addAction( BluetoothDevice.ACTION_ACL_DISCONNECTED  );
        _activity.registerReceiver( _bluetoothStatusReceiver, filter );
        _intentBTStatusRegistered = true;
      }
    }
    return ans;
  }
   
  /// true if an enabling request is beeing performed
  private boolean _bluetoothRequested = false;
  /**
   *  Display a window requesting the bluetooth
   */
  private void requestBluetooth()
  {
    if( _bluetoothRequested == false )
    {
      _bluetoothRequested = true;
      Intent enableIntent = new Intent(_bluetoothAdapter.ACTION_REQUEST_ENABLE);
      _activity.startActivityForResult(enableIntent, Recon.ACTIVATE_BLUETOOTH);
    }
  }
  
  /**
   *  Called when the window requesting bluetooth activation returns
   */
  public void answerBluetoothRequest( boolean activated )
  {
    _bluetoothRequested = false;
    if( activated )
      bluetoothActivated();
  }
  
  /**
   *  Checks if the bluetooth is enabled, if not, the Activity will be asked to start it
   *  @return false if the bluetooth is not enabled, true otherwise
   */
  private boolean checkBluetoothEnabled()
  {
    boolean ans = true;
    
    Log.d( _logTag, "checkBluetoothEnabled");
    
    if( !_bluetoothAdapter.isEnabled() )
    {
      requestBluetooth();
      ans = false;
    }
    return ans;
  }

  /**
   *  handles the errors
   */
  synchronized private void handleError( Message msg, IOException e, String value)
  {
    Log.e(_logTag, value, e);

    msg.sendToTarget();
  }
  
  // reconnect to the last client
  public void reconnect()
  {
    if(_lastConnectedAddr != null)
      setBluetoothDeviceFromAddress( _lastConnectedAddr );
  }
  
  public void setBluetoothDeviceFromAddress( String inAddress )
  {
    _lastAttemptAddr = inAddress;
    _client.connect( _lastAttemptAddr, _serviceUUID, _bluetoothAdapter );
  }
  
  /**
   * Initiate the bluetooth connection
   */
  synchronized private void initBluetooth()
  {
    boolean ok = true;
    
    Log.d( _logTag, "initBluetooth");
    
    switch( _initializationStep ){
    case NOT_INITIALIZED:
    case GET_BLUETOOTH_ADAPTER :
      if( ok )
      {
        _initializationStep = InitStep.GET_BLUETOOTH_ADAPTER;
        ok = getBluetoothAdapter();
      }
        
    case CHECK_BLUETOOTH_ENABLED :
      if( ok )
      {
        _initializationStep = InitStep.CHECK_BLUETOOTH_ENABLED;
        ok = checkBluetoothEnabled();
      }
        
    case INIT_FINISHED :
      if( ok )
      {
        _initializationStep = InitStep.INIT_FINISHED;
      }
    default :
      break;
    }
  }
  
  public void writeData( byte[] buffer )
  {

    int packetLength = buffer.length + SIZE_BYTES + 1;
    byte[] dataToWrite = new byte[ packetLength ];
    ByteBuffer byteBuffer = ByteBuffer.allocate(SIZE_BYTES);
    byteBuffer.putInt( packetLength );
    
    // adds the size of the packet
    for (int i = 0; i < SIZE_BYTES; ++i )
      dataToWrite[i] = byteBuffer.array()[i];
    
    // adds the data
    for( int i = 0; i < buffer.length; ++i )
      dataToWrite[ i + SIZE_BYTES ] = buffer[i];
    
    // adds the final end of line
    dataToWrite[ dataToWrite.length-1 ] = '\n';

    String sMsg = "";
    for(int i = 0; i < dataToWrite.length; ++i)
      sMsg = sMsg + (char)dataToWrite[i];
    sMsg = sMsg + " size " + dataToWrite.length;
    Log.d( _logTag, sMsg);

    try{
      _client.write( dataToWrite );
    } catch (IOException e) {
        Log.e( _logTag, "writing error", e);
        _client.close();
    }
  }

  public void writeData( String buffer )
  {
    writeData( buffer.getBytes() );
  }

  public void closeBluetooth()
  {
    switch( _initializationStep )
    {
    case INIT_FINISHED :
      _client.close();
    case CHECK_BLUETOOTH_ENABLED :
    case GET_BLUETOOTH_ADAPTER :
    case NOT_INITIALIZED :
    default :
      break;
    }
    _initializationStep = InitStep.CHECK_BLUETOOTH_ENABLED;
    _handler.obtainMessage( ReconBluetoothHandler.MESSAGE_CLOSED, 0, -1 ).sendToTarget();
    Log.d( _logTag, "Recon Connection Closed");
  }
  
  /** adds the incoming data in the incoming buffer
   *  @param[in] inBuffer : received data
   *  @param[in] inSize : size of the received data
   */
  private void addIncomingBuffer( byte[] inBuffer, int inSize )
  { 
    if( _incomingData != null )
    {
      byte[] newArray = new byte[_incomingData.length + inSize];
      System.arraycopy(_incomingData, 0, newArray, 0, _incomingData.length );
      System.arraycopy(inBuffer, 0, newArray, _incomingData.length, inSize );
      _incomingData = newArray;
    }
    else
    {
      _incomingData = new byte[inSize];
      System.arraycopy(inBuffer, 0, _incomingData, 0, inSize );
    }
  }

  /** Gets the size of the packet
   * @param[in] inBuffer : buffer to analyse
   * @return number of bytes of the packet
   */
  private int getPacketSize( byte[] inBuffer)
  {
    int ans = 0;
    // checks if the buffer is large enough
    if( inBuffer.length >= SIZE_BYTES )
    {
      // data size (4 bytes) is complete and can be calculated
      for ( int i = 0; i < SIZE_BYTES; ++i )
         ans = ( ans << 8 ) + ( inBuffer[i] & 0xff );
    }
    return ans;
  }

  synchronized private void packetHandler( byte[] inBuffer, int inSize )
  {
    addIncomingBuffer(inBuffer, inSize);
    boolean ok = true;
    
    while(ok)
    {
      ok = false;
      if( _incomingPacketSize == 0 )
        _incomingPacketSize = getPacketSize( _incomingData );
      
      if( _incomingPacketSize > MAX_SIZE || _incomingPacketSize < SIZE_BYTES + 1 )
      {
        Log.d( _logTag, "wrong packet size : " + _incomingPacketSize );
        _incomingData = null;
        _incomingPacketSize = 0;
      }
      else
      {
        if( _incomingData.length >= _incomingPacketSize )
        {
          ok = true;
          byte[] completeData = null;
          
          // gets the packet data (whole data without size and final line feed)
          completeData = new byte[ _incomingPacketSize - SIZE_BYTES - 1];
          System.arraycopy(_incomingData, SIZE_BYTES, completeData, 0, completeData.length);

          if( _incomingData.length == _incomingPacketSize )
          {
            _incomingData = null;
            _incomingPacketSize = 0;
            ok = false;
          }
          else
          {
            // gets the remaining data
            byte[] remainingData = new byte[_incomingData.length - _incomingPacketSize];
            System.arraycopy(_incomingData, _incomingPacketSize, remainingData, 0, remainingData.length );
            _incomingData = remainingData;
            _incomingPacketSize = 0;
          }
          _handler.obtainMessage( ReconBluetoothHandler.MESSAGE_READ_MSG, -1, -1, completeData )
                  .sendToTarget();
        }
      }
    }
  }
}

package org.bigfoot.recon;

import android.app.Activity;

import android.os.Handler;
import android.os.Message;

import android.widget.Toast;
import android.graphics.Color;

import android.util.Log;

public class ReconBluetoothHandler extends Handler 
{
  public static final int MESSAGE_READY = 1;
  public static final int MESSAGE_FAILED = 2;
  public static final int MESSAGE_NO_BLUETOOTH = 3;
  public static final int MESSAGE_ACTIVATE_BLUETOOTH = 4;
  public static final int MESSAGE_CLOSE_FAILED = 5;
  public static final int MESSAGE_CLOSED = 6;
  public static final int MESSAGE_CONNECTING_FAILED = 7;
  public static final int MESSAGE_WRITE_FAILED = 8;
  public static final int MESSAGE_CONNECTION_LOST = 9;
  public static final int MESSAGE_CONNECTED = 10;
  public static final int MESSAGE_READ_MSG = 11;
  public static final int MESSAGE_CONNECTING = 12;
   
  /// called when the bluetooth states changed
  protected void onStatusChange( String inText, int inColor )
  {
  }
  
  /// called when a message must be printed
  protected void onMessageDisplay( String inText)
  {
  }
  
  /// called when receiving a bluetooth message
  protected void onBluetoothReception( byte[]data )
  {
  }
  
  public ReconBluetoothHandler()
  {
  }

  @Override
  public void handleMessage(Message msg)
  {
    String message = "";
    switch( msg.what )
    {
      case MESSAGE_READY:
      case MESSAGE_CONNECTION_LOST:
        message = "Disconnected";
        onStatusChange( message, Color.YELLOW );
        break;
      case MESSAGE_FAILED:
        message = "Failed: " + (String) msg.obj;
        break;
      case MESSAGE_NO_BLUETOOTH:
        message = "no bluetooth available";
        break;
      case MESSAGE_CLOSE_FAILED:
        message = "closing error: " + (String) msg.obj;
        break;
      case MESSAGE_CLOSED:
        message = "Recon Closed";
        break;
      case MESSAGE_WRITE_FAILED:
        message = "Write failed";
        break;
      case MESSAGE_CONNECTED:
        message = "Connected";
        onStatusChange( "Connected", Color.GREEN );
        break;
      case MESSAGE_CONNECTING:
        message = "";
        onStatusChange( "Connecting", Color.RED );
        break;
      case MESSAGE_CONNECTING_FAILED:
        message = "Connection Failed";
        onStatusChange( "Disconnected", Color.YELLOW );
        break;
      case MESSAGE_READ_MSG:
        message = "";
        byte[] readBuf = (byte[]) msg.obj;
        onBluetoothReception( readBuf );
        break;
      default:
        message = "unknown value";
    }
    if( message != "" )
      onMessageDisplay( message );
  }
}

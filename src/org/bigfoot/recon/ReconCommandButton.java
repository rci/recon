package org.bigfoot.recon;

import java.lang.reflect.Method;
import java.lang.IllegalStateException;
import java.lang.reflect.InvocationTargetException;

import android.content.Context;

import android.util.AttributeSet;

import android.content.res.TypedArray;

import android.view.View;

import android.widget.ImageButton;

public class ReconCommandButton extends ImageButton
{
  private String _argument = "";
  private String _action = "";
	public ReconCommandButton(Context context, AttributeSet attrs)
	{
	  super(context, attrs);
	  setAttributes(context, attrs);
	}
	
	public ReconCommandButton(Context context, AttributeSet attrs, int defStyle)
	{
	  super(context, attrs, defStyle);
	  setAttributes(context, attrs);
	}
	
	protected void setAttributes(Context context, AttributeSet attrs)
	{
	  TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ReconCommandButton);
    final int N = a.getIndexCount();
    for (int i = 0; i < N; ++i)
    {
      int attr = a.getIndex(i);
      switch (attr)
      {
        case R.styleable.ReconCommandButton_argument:
            _argument = a.getString(attr);
            break;
        case R.styleable.ReconCommandButton_action:
            _action = a.getString(attr);
            break;
      }
    }
    a.recycle();
	}
	
	public String getAction() {return _action;}
	public String getArgument() {return _argument;}
}

